# -*- coding: utf-8 -*-

from app import db

class Task(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	title = db.Column(db.String(64))
	description = db.Column(db.String(256))
	done = db.Column(db.Boolean, default = False)
	
	def __repr__(self):
		return '<Task %r: %r>' % (self.id, self.title)